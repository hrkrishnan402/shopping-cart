import 'package:demo/api/api.dart';
import 'package:demo/api/productdetails.dart';
import 'package:demo/cartpage/cartpage.dart';
import 'package:demo/provider/provider.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class DetailsPage extends StatefulWidget {
  const DetailsPage({ Key key, this.productId }) : super(key: key);
  final String productId;

  @override
  _DetailsPageState createState() => _DetailsPageState();
}

class _DetailsPageState extends State<DetailsPage> {
  AppProvider _appProvider =AppProvider();
  ProductDetails _productDetails;
  Attrs attrs;
  List<Spec> spec;
  bool imageRecieved = false;
  @override
    void initState() {
      fetchData();
      super.initState();
    }
  @override
  Widget build(BuildContext context) {
    return Scaffold(appBar: AppBar(
      leading: Icon(Icons.arrow_back),
      centerTitle: true,
      title: Text("Item Details"),
      actions: [
      Icon(Icons.search),
      IconButton(icon: Icon(Icons.shopping_cart_outlined),
      onPressed: (){
        Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => CartPage()),
  );
      },)
    ],),
    body: (!imageRecieved) ? Center(child: CircularProgressIndicator(),) : SingleChildScrollView(child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
     Padding(padding: EdgeInsets.all(4),
     child: Image.network(_productDetails.image[0],
      height: MediaQuery.of(context).size.height * 0.5,)),
      hardwareDetails(),
      productDetails(),
      addButton()
    ],),)
    );
  }

  Widget hardwareDetails(){
    return Card(child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
      Container(
        color: Colors.grey[300],
        child: Row(
        children: [
        Padding(padding: EdgeInsets.symmetric(horizontal: 10),
         child: Text("color"),),
        Padding(padding: EdgeInsets.symmetric(vertical: 8),
        child: Image.network(attrs.color,
        height: 20,
        width: 30,),),
      ],),),
      Wrap(
        children: spec.map((Spec e) => iconBox(e.title, e.value)).toList(),)
    ],),);
  }
  Widget productDetails(){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
      Text(_productDetails.name),
      Chip(
        backgroundColor: Colors.yellow[800],
        label: Container(
          width: 50,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
        Icon(Icons.star),
        Text("3.4")
      ],),),
        )
    ],);
  }
  Widget addButton(){
    Item item;
    return Center(child: ElevatedButton(child: Text("ADD TO CART"),
    onPressed: (){
      for (var i = 0; i < _appProvider.items.length; i++) {
        if(_appProvider.items[i].id == _productDetails.id){
          item = _appProvider.items[i];
        }
      }
      if (item != null ) {_appProvider.addtoCart(item);}
      print(_appProvider.cartItems);
    },
    style: ButtonStyle(backgroundColor: MaterialStateProperty.all<Color>(Colors.red)),),);
  }

  Widget iconBox(String iconData, String description){
  return Container(
    width: MediaQuery.of(context).size.width * 0.3,
    child: Column(children: [
      if(iconData == "battery")
         Icon(Icons.battery_full,
        size: 50,),
      if(iconData == "camera")
         Icon(Icons.camera,
        size: 50,),
      if(iconData == "memory")
         Icon(Icons.memory,
        size: 50,),
      if(iconData == "storage")
         Icon(Icons.storage,
        size: 50,),
      if(iconData == "screen")
         Icon(Icons.mobile_screen_share_sharp,
        size: 50,),
      Text(description)
    ],)
  );
  }
  void fetchData() async{
    String id=widget.productId;
    final response = await http.get("http://omanphone.smsoman.com/api/productdetails?id=$id");
    _productDetails = productDetailsFromJson(response.body);
    attrs = _productDetails.attrs;
    spec = attrs.specs;
    setState(() {
          imageRecieved = true;
        });
  }

  Widget iconData(String iconName){
    switch (iconName) {
      case "battery":
        return Icon(Icons.battery_full,
        size: 50,);
        break;
      case "camera":
        return Icon(Icons.camera,
        size: 50,);
        break;
      case "screen":
        return Icon(Icons.mobile_screen_share_sharp,
        size: 50,);
        break;
      case "memory":
        return Icon(Icons.memory,
        size: 50,);
        break;
      case "storage":
        return Icon(Icons.storage,
        size: 50,);
      break;
      default: return Icon(Icons.battery_full,
        size: 50,);
    }
  }
}