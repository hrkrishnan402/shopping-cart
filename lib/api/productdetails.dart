import 'package:meta/meta.dart';
import 'dart:convert';

ProductDetails productDetailsFromJson(String str) => ProductDetails.fromJson(json.decode(str));

String productDetailsToJson(ProductDetails data) => json.encode(data.toJson());

class ProductDetails {
    ProductDetails({
        @required this.name,
        @required this.id,
        @required this.sku,
        @required this.evoucher,
        @required this.shortDescription,
        @required this.brand,
        @required this.image,
        @required this.hasOptions,
        @required this.productTag,
        @required this.preorder,
        @required this.preorderinfo,
        @required this.price,
        @required this.specialPrice,
        @required this.status,
        @required this.attrs,
    });

    final String name;
    final String id;
    final String sku;
    final int evoucher;
    final dynamic shortDescription;
    final String brand;
    final List<String> image;
    final int hasOptions;
    final String productTag;
    final String preorder;
    final Preorderinfo preorderinfo;
    final double price;
    final int specialPrice;
    final int status;
    final Attrs attrs;

    factory ProductDetails.fromJson(Map<String, dynamic> json) => ProductDetails(
        name: json["name"],
        id: json["id"],
        sku: json["sku"],
        evoucher: json["evoucher"],
        shortDescription: json["short_description"],
        brand: json["brand"],
        image: List<String>.from(json["image"].map((x) => x)),
        hasOptions: json["has_options"],
        productTag: json["product_tag"],
        preorder: json["preorder"],
        preorderinfo: Preorderinfo.fromJson(json["preorderinfo"]),
        price: json["price"].toDouble(),
        specialPrice: json["special_price"],
        status: json["status"],
        attrs: Attrs.fromJson(json["attrs"]),
    );

    Map<String, dynamic> toJson() => {
        "name": name,
        "id": id,
        "sku": sku,
        "evoucher": evoucher,
        "short_description": shortDescription,
        "brand": brand,
        "image": List<dynamic>.from(image.map((x) => x)),
        "has_options": hasOptions,
        "product_tag": productTag,
        "preorder": preorder,
        "preorderinfo": preorderinfo.toJson(),
        "price": price,
        "special_price": specialPrice,
        "status": status,
        "attrs": attrs.toJson(),
    };
}

class Attrs {
    Attrs({
        @required this.color,
        @required this.specs,
    });

    final String color;
    final List<Spec> specs;

    factory Attrs.fromJson(Map<String, dynamic> json) => Attrs(
        color: json["color"],
        specs: List<Spec>.from(json["specs"].map((x) => Spec.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "color": color,
        "specs": List<dynamic>.from(specs.map((x) => x.toJson())),
    };
}

class Spec {
    Spec({
        @required this.value,
        @required this.icon,
        @required this.title,
    });

    final String value;
    final String icon;
    final String title;

    factory Spec.fromJson(Map<String, dynamic> json) => Spec(
        value: json["value"],
        icon: json["icon"],
        title: json["title"],
    );

    Map<String, dynamic> toJson() => {
        "value": value,
        "icon": icon,
        "title": title,
    };
}

class Preorderinfo {
    Preorderinfo({
        @required this.preorderType,
        @required this.preorderPercentage,
        @required this.preorderMsg,
        @required this.freePreorderNote,
        @required this.preOrderQty,
        @required this.isPreorderProduct,
        @required this.availabilityOn,
    });

    final String preorderType;
    final String preorderPercentage;
    final String preorderMsg;
    final String freePreorderNote;
    final String preOrderQty;
    final String isPreorderProduct;
    final String availabilityOn;

    factory Preorderinfo.fromJson(Map<String, dynamic> json) => Preorderinfo(
        preorderType: json["preorderType"],
        preorderPercentage: json["preorderPercentage"],
        preorderMsg: json["preorderMsg"],
        freePreorderNote: json["freePreorderNote"],
        preOrderQty: json["preOrderQty"],
        isPreorderProduct: json["isPreorderProduct"],
        availabilityOn: json["availabilityOn"],
    );

    Map<String, dynamic> toJson() => {
        "preorderType": preorderType,
        "preorderPercentage": preorderPercentage,
        "preorderMsg": preorderMsg,
        "freePreorderNote": freePreorderNote,
        "preOrderQty": preOrderQty,
        "isPreorderProduct": isPreorderProduct,
        "availabilityOn": availabilityOn,
    };
}
