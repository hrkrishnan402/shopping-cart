// To parse this JSON data, do
//
//     final welcome = welcomeFromJson(jsonString);

import 'package:meta/meta.dart';
import 'dart:convert';

List<Welcome> welcomeFromJson(String str) => List<Welcome>.from(json.decode(str).map((x) => Welcome.fromJson(x)));

String welcomeToJson(List<Welcome> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Welcome {
    Welcome({
        @required this.type,
        @required this.data,
        @required this.subtype,
    });

    final String type;
    final Data data;
    final String subtype;

    factory Welcome.fromJson(Map<String, dynamic> json) => Welcome(
        type: json["type"],
        data: Data.fromJson(json["data"]),
        subtype: json["subtype"] == null ? null : json["subtype"],
    );

    Map<String, dynamic> toJson() => {
        "type": type,
        "data": data.toJson(),
        "subtype": subtype == null ? null : subtype,
    };
}

class Data {
    Data({
        @required this.id,
        @required this.title,
        @required this.items,
        @required this.type,
        @required this.file,
    });

    final String id;
    final String title;
    final List<Item> items;
    final String type;
    final String file;

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        id: json["id"],
        title: json["title"] == null ? null : json["title"],
        items: json["items"] == null ? null : List<Item>.from(json["items"].map((x) => Item.fromJson(x))),
        type: json["type"] == null ? null : json["type"],
        file: json["file"] == null ? null : json["file"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "title": title == null ? null : title,
        "items": items == null ? null : List<dynamic>.from(items.map((x) => x.toJson())),
        "type": type == null ? null : type,
        "file": file == null ? null : file,
    };
}

class Item {
    Item({
        @required this.name,
        @required this.id,
        @required this.sku,
        @required this.image,
        @required this.price,
        @required this.specialPrice,
        @required this.rating,
        @required this.storage,
        @required this.productTag,
        @required this.preorder,
    });

    final String name;
    final String id;
    final String sku;
    final String image;
    final double price;
    final int specialPrice;
    final String rating;
    final dynamic storage;
    final String productTag;
    final String preorder;

    factory Item.fromJson(Map<String, dynamic> json) => Item(
        name: json["name"],
        id: json["id"],
        sku: json["sku"],
        image: json["image"] == null ? null : json["image"],
        price: json["price"].toDouble(),
        specialPrice: json["special_price"] == null ? null : json["special_price"],
        rating: json["rating"] == null ? null : json["rating"],
        storage: json["storage"],
        productTag: json["product_tag"] == null ? null : json["product_tag"],
        preorder: json["preorder"] == null ? null : json["preorder"],
    );

    Map<String, dynamic> toJson() => {
        "name": name,
        "id": id,
        "sku": sku,
        "image": image == null ? null : image,
        "price": price,
        "special_price": specialPrice == null ? null : specialPrice,
        "rating": rating == null ? null : rating,
        "storage": storage,
        "product_tag": productTag == null ? null : productTag,
        "preorder": preorder == null ? null : preorder,
    };
}

enum StorageEnum { THE_128_GB }

final storageEnumValues = EnumValues({
    "128 GB": StorageEnum.THE_128_GB
});

class EnumValues<T> {
    Map<String, T> map;
    Map<T, String> reverseMap;

    EnumValues(this.map);

    Map<T, String> get reverse {
        if (reverseMap == null) {
            reverseMap = map.map((k, v) => new MapEntry(v, k));
        }
        return reverseMap;
    }
}
