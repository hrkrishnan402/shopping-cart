import 'package:demo/api/api.dart';
import 'package:demo/detailspage/detailspage.dart';
import 'package:demo/provider/provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class HomePage extends StatefulWidget {
  const HomePage({ Key key }) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  AppProvider _appProvider = AppProvider();
  bool dataLoading = false;
  @override
    void initState() {
      super.initState();
      fetchData();
    }

  @override
  Widget build(BuildContext context) {
    return Scaffold(appBar: AppBar(
      bottom: PreferredSize(
        preferredSize: Size.fromHeight(4.0),
        child: TextField(
        decoration: InputDecoration(
        border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      filled: true,
      hintStyle: TextStyle(color: Colors.grey[800]),
      hintText: "Type in your text",
      fillColor: Colors.white70),
),),
        leading: Icon(Icons.menu),
        centerTitle: true,
        title: Text("OMAN PHONE"),
        actions: [
          Icon(Icons.notification_important)
        ],
    ),
    body: (!dataLoading) ? CircularProgressIndicator() : GridView.builder(
      itemCount: _appProvider.items.length,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
      itemBuilder: (BuildContext context, int index) {
          return itemsGrid(_appProvider.items[index]);
      }
        ),
    bottomSheet: Row(children: [
      Icon(Icons.home)
    ],),);
  }

  Widget itemsGrid(Item item){
    return GestureDetector(
      onTap: (){
        Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => DetailsPage(productId: item.id,)),
            );
      },
      child: Card(
      child: Column(
        children: [
        // Image.network(item.image) didnt got the image properly, but can view in details page,
        Expanded(child: Icon(Icons.phone,
        size: MediaQuery.of(context).size.height * 0.15,),),
        Text(item.name),
        Text(item.price.toString()),
      ],),
    ),);
  }

  void fetchData() async{
    http.Response response = await http.get("https://omanphone.smsoman.com/api/homepage");
    String jsonString = response.body;
    List<Welcome> welcome = welcomeFromJson(jsonString);
    Data data = welcome[0].data;
    _appProvider.setItemList(data.items);
    setState(() {
          dataLoading = true;
        });
  }
}