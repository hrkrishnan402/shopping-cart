import 'package:demo/api/api.dart';
import 'package:flutter/material.dart';
class AppProvider extends ChangeNotifier{
  static final AppProvider _singleton = AppProvider._internal();
  factory AppProvider() => _singleton;
  AppProvider._internal();

    List<Welcome> welcome = [];
    List<Item> items = [];
    List<Item> cartItems = [];

    void setItemList(List<Item> items){
      this.items = items;
    }

    void addtoCart(Item item){
      this.cartItems.add(item);
    }
}