import 'package:demo/api/api.dart';
import 'package:demo/provider/provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CartPage extends StatefulWidget {
  CartPage({ Key key }) : super(key: key);


  @override
  _CartPageState createState() => _CartPageState();
}

class _CartPageState extends State<CartPage> {
  AppProvider _appProvider = AppProvider();

  @override
  Widget build(BuildContext context) {
    return Scaffold(appBar:  AppBar(
      leading: Icon(Icons.arrow_back),
      centerTitle: true,
      title: Text("My Cart"),),
      body: SingleChildScrollView(child:
        _appProvider.cartItems.isEmpty ? Center(child: Text("No items"),) :
       Column(children: _appProvider.cartItems.map((Item item) => 
       productDeatils(item)).toList(),),));
  }

  Widget productDeatils(Item item){
    return Card(child: Column(children: [
      Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
        Expanded(child: Padding(padding: EdgeInsets.symmetric(vertical: 10),
        child: Text(item.name,),),),
        Column(children: [
          /* image path was not working properly */
          Image.network("https://m.media-amazon.com/images/I/71uuDYxn3XL.jpg",
        width: 110,
        height: 200,
        fit: BoxFit.fitHeight,),
        Row(children: [
          circleButton(Icons.add),
          Text("1"),
          circleButton(Icons.remove)
        ],)
        ],)
      ],),
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
        Text(item.price.toString()),
        Icon(Icons.delete),
      ],)
    ],),);
  }
  Widget circleButton(IconData iconData){
    return RawMaterialButton(
  onPressed: () {},
  elevation: 2.0,
  fillColor: Colors.white,
  child: Icon(
    iconData,
  ),
  shape: CircleBorder(),
);
  }
}